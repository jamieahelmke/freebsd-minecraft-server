# FreeBSD Minecraft Server

This repository provides documentation and a rc script to run a Vanilla Minecraft: Java Edition server on FreeBSD.

Setup Steps:
1. Install a Java Runtime Environment (JRE) on your system. (Here)[https://www.freebsd.org/de/java/] is the official FreeBSD Documentation to it.

2. Install a Version of your Minecraft Server Jar in the following directory: `/usr/local/minecrafd`. The newest version of it is available (here)[https://www.minecraft.net/en-us/download/server].

3. You should start the server now with following command: `java -Xmx4096M -Xms1024M -jar /usr/local/minecraftd/server.jar nogui`. This will generate the necessary files used for the server.

4. You will get prompted to accept the eula. Just edit the eula file and set it to "true".

5. Create a user and :group `minecraftd` and give it full access to `/usr/local/minecraftd`. 

6. Install the provided `minecraftd` script to the `/etc/rc.d` directory.

7. Add line `minecraftd_enable="YES"` in `/etc/rc.conf`

8. Execute `service minecraftd start` to start the server
**Tip:** If you want access to the server console, run the manual command above.

To configure how much RAM the minecraft server can use, edit the value here:

```
java -XmX4096M -Xms1024M -jar /usr/local/minecrafd/server.jar nogui
         ^^^^
         here
```
